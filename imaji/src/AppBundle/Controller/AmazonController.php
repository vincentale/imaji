<?php
/**
 * Created by PhpStorm.
 * User: vincentale
 * Date: 29/11/17
 * Time: 13:49
 */

namespace AppBundle\Controller;


use Aws\S3\S3Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/amazon")
 * Class AmazonController
 * @package AppBundle\Controller
 */
class AmazonController extends Controller
{
    /**
     * @Route("/", name="amazon_upload")
     */
    public function uploadAction(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('images', FileType::class)
            ->add('Submit', SubmitType::class)// If I remove this line data is submitted correctly
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $image = reset($data);
            $fileName = md5(uniqid()).'.'.$image->guessExtension();
            $filePath = $this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . "upload";
            $image->move($filePath , $fileName);


            $bucket = 'traitement-image';
            $keyname = $fileName;
// $filepath should be absolute path to a file on disk
            $filepath = $filePath;

// Instantiate the client.
            $s3 = new S3Client(['version' => 'latest',
                'region' => 'eu-central-1',
                'credentials' => [
                    'key' => 'AKIAJVI56WDQT7GQME5Q',
                    'secret' => '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'
                ]]);

// Upload a file.
            $result = $s3->putObject(array(
                'Bucket'       => $bucket,
                'Key'          => $keyname,
                'SourceFile'   => $filepath . DIRECTORY_SEPARATOR . $fileName,
                'ContentType'  => 'image/jpeg',
                'ACL'          => 'public-read',
                'StorageClass' => 'REDUCED_REDUNDANCY',
                'Metadata'     => array(
                    'param1' => 'value 1',
                    'param2' => 'value 2'
                )
            ));

            return $this->render('default/amazon.html.twig', array(
                'form' => $form->createView(),
                'link' => $result['ObjectURL']
            ));
        }
        else {
            return $this->render('default/amazon.html.twig', array(
                'form' => $form->createView(),
            ));
        }
    }
}