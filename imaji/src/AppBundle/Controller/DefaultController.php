<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TreatmentEntry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="upload")
     */
    public function uploadAction(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('images', FileType::class)
            ->add('Submit', SubmitType::class)// If I remove this line data is submitted correctly
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            foreach ($data as $image)
            {
                $fileName = md5(uniqid()).'.'.$image->guessExtension();
                $image->move($this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . $this->getParameter('upload_dir'), $fileName);
                $entry = new TreatmentEntry();
                $entry->setImageName($fileName);
                $this->getDoctrine()->getManager()->persist($entry);
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Image successfuly uploaded !");
            return $this->redirectToRoute("crop", ["filename" => $fileName]);
        }
        else {
            $treatmentEntries = $this->getDoctrine()->getManager()->getRepository(TreatmentEntry::class)->findBy([],[], 10);
            return $this->render('default/index.html.twig', array(
                'form' => $form->createView(),
                'entries' => $treatmentEntries
            ));
        }

    }

    /**
     * @Route("/download/{filename}", name="download")
     * @param $filename
     */
    public function downloadAction($filename)
    {
        $fullPath = $this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . "../web/filtered" .DIRECTORY_SEPARATOR . $filename;
        return new BinaryFileResponse($fullPath);
    }

    /**
     * @Route("/show/{filename}", name="show")
     */
    public function showAction($filename)
    {
        return $this->render('default/show.html.twig', array(
            'filename' => $filename,
        ));
    }
}
