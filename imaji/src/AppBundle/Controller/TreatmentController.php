<?php
/**
 * Created by PhpStorm.
 * User: vincentale
 * Date: 27/11/17
 * Time: 14:40
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TreatmentController extends Controller
{
    /**
     * @Route("/grey/{filename}", name="grey")
     *
     * @param Request $request
     */
    public function greyAction($filename)
    {
        $fs = new Filesystem();
        $fullPath = $this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . "../web/filtered" .DIRECTORY_SEPARATOR . $filename;
        if (!$fs->exists($fullPath)) {
            $oldPath = $this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . $this->getParameter("upload_dir") .DIRECTORY_SEPARATOR . $filename;
            $fs->copy($oldPath, $fullPath);
        }
        $files = new \Imagick($fullPath);
        $files->transformImageColorspace(\Imagick::COLORSPACE_GRAY);
        $files->getImageBlob();
        try {;
            $fs->dumpFile($fullPath,  $files->getImageBlob());
        } catch (IOExceptionInterface $e) {
            $this->addFlash("danger", "An error orccured");
            return $this->redirectToRoute("upload");
        }
        return $this->redirectToRoute("blur", ["filename" => $filename]);
    }

    /**
     * @Route("/blur/{filename}", name="blur")
     *
     * @param Request $request
     */
    public function blurAction($filename)
    {
        $fs = new Filesystem();
        $fullPath = $this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . "../web/filtered" .DIRECTORY_SEPARATOR . $filename;
        if (!$fs->exists($fullPath)) {
            $oldPath = $this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . $this->getParameter("upload_dir") .DIRECTORY_SEPARATOR . $filename;
            $fs->copy($oldPath, $fullPath);
        }
        $files = new \Imagick($fullPath);
        $files->blurImage(12, 3);
        $files->getImageBlob();
        try {
            $fs->dumpFile($fullPath,  $files->getImageBlob());
        } catch (IOExceptionInterface $e) {
            $this->addFlash("danger", "An error orccured");
            return $this->redirectToRoute("upload");
        }
        return $this->redirectToRoute("show", ['filename' => $filename]);
    }

    /**
     * @Route("/crop/{filename}", name="crop")
     *
     * @param Request $request
     */
    public function cropAction($filename)
    {
        $fs = new Filesystem();
        $fullPath = $this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . "../web/filtered" .DIRECTORY_SEPARATOR . $filename;
        if (!$fs->exists($fullPath)) {
            $oldPath = $this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . $this->getParameter("upload_dir") .DIRECTORY_SEPARATOR . $filename;
            $fs->copy($oldPath, $fullPath);
        }
        $files = new \Imagick($fullPath);
        $geometry = $files->getImageGeometry();
        $width = $geometry['width'];
        $height = $geometry['height'];
        $files->cropImage($width-50,$height-40, 50, 40);
        $files->getImageBlob();
        $fs = new Filesystem();
        try {
            $fs->dumpFile($fullPath,  $files->getImageBlob());
        } catch (IOExceptionInterface $e) {
            $this->addFlash("danger", "An error orccured");
            return $this->redirectToRoute("upload");
        }
        return $this->redirectToRoute("grey", ["filename" => $filename]);
    }
}