<?php
/**
 * Created by PhpStorm.
 * User: vincentale
 * Date: 27/11/17
 * Time: 15:24
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="treatment_entry")
 * @ORM\HasLifecycleCallbacks()
 * Class TreatmentEntry
 * @package AppBundle\Entity
 */
class TreatmentEntry
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="integer", name="count")
     */
    private $treatmentCount = 0;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param mixed $imageName
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->setCreatedAt(new \DateTime());

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTreatmentCount()
    {
        return $this->treatmentCount;
    }



    /**
     * @param mixed $treatmentCount
     */
    public function setTreatmentCount($treatmentCount)
    {
        $this->treatmentCount = $treatmentCount;

        return $this;
    }

}